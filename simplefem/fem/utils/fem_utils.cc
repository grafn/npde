/**
 * @file
 * @brief NPDE neumann bvp utils for io handling
 * @author Nico Graf
 */
#include "fem_utils.h"

namespace FemUtils {

template <typename Derived>
std::istream &operator>>(std::istream &is, Eigen::MatrixBase<Derived> &matrix) {
  for (int i = 0; i < matrix.rows(); ++i) {
    for (int j = 0; j < matrix.cols(); ++j) {
      is >> matrix(i, j);
    }
  }
  return is;
}

std::shared_ptr<lf::mesh::Mesh> read_mesh(std::string filename) {
  std::ifstream file(filename, std::ifstream::in);

  if (file.is_open()) {
    // read vertices
    int n_vertices;
    std::string vertices_label;
    file >> n_vertices >> vertices_label;
    if (vertices_label != "Vertices") {
      file.close();
      throw std::runtime_error(
          "Keyword 'Vertices' not found. Wrong file format.");
    }
    Eigen::Matrix<double, Eigen::Dynamic, 2> _nodecoords = Eigen::MatrixXd(n_vertices, 2);
    file >> _nodecoords;

    // read elements
    int n_elements;
    std::string elements_label;
    file >> n_elements >> elements_label;
    if (elements_label != "Elements") {
      file.close();
      throw std::runtime_error(
          "Keyword 'Elements' not found. Wrong file format.");
    }
    Eigen::Matrix<unsigned int, Eigen::Dynamic, 3> _elements(n_elements, 3);
    file >> _elements;
    lf::mesh::hybrid2d::MeshFactory factory(2);
    for(int i = 0; i < _nodecoords.rows(); i++){
      factory.AddPoint(_nodecoords.row(i));
    }
    for(int i = 0; i < _elements.rows(); i++){
      const std::array<unsigned int, 3> triag = {_elements(i,0), _elements(i,1), _elements(i,2)};
      factory.AddEntity(lf::base::RefEl::kTria(), nonstd::span(triag.data(), 3), nullptr);
    }
    
    file.close();
    
    return factory.Build();
  } else {
    throw std::runtime_error("Error when opening file '" + filename + "'.");
  }
  
  return nullptr;
}

void generate_solutions(std::string filename, Eigen::VectorXd solution, std::shared_ptr<lf::fe::ScalarFESpace<double>> fe_space_p ){
 
  // data stored with codim=0 entities
  std::shared_ptr<lf::mesh::utils::MeshDataSet<double>> cell_data;
 
  // vector data stored with points of the mesh
  std::shared_ptr<lf::mesh::utils::MeshDataSet<Eigen::Vector2d>> point_data;
  
  // create mesh functions representing solution
  auto mf_sol = lf::fe::MeshFunctionFE<double, double>(fe_space_p, solution);
 
  lf::io::VtkWriter vtk_writer(fe_space_p->Mesh(), filename);
  vtk_writer.WritePointData("mfPoint", mf_sol);
  
}

} // namespace FemUtils