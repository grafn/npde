/**
 * @file
 * @brief NPDE neumann bvp utils for io handling
 * @author Nico Graf
 */

#ifndef FEM_UTILS_FEM_UTILS_H
#define FEM_UTILS_FEM_UTILS_H

#include <lf/base/span.h>
#include <array>
#include <cmath>
#include <lf/fe/scalar_fe_space.h>
#include <memory>
#include <vector>
#include <Eigen/Core>
#include <lf/geometry/geometry.h>
#include <string>
#include <cassert>
#include <fstream>
#include <iostream>
#include <lf/mesh/mesh.h>
#include <lf/mesh/hybrid2d/mesh_factory.h>
#include <lf/base/base.h>
#include <lf/mesh/utils/utils.h>
#include <lf/fe/fe.h>
#include <lf/io/io.h>

namespace FemUtils {

template <typename Derived>
std::istream &operator>>(std::istream &is, Eigen::MatrixBase<Derived> &matrix);

std::shared_ptr<lf::mesh::Mesh> read_mesh(std::string filename);

void generate_solutions(std::string filename, Eigen::VectorXd solution, std::shared_ptr<lf::fe::ScalarFESpace<double>> fe_space_p );


} // namespace FemUtils

#endif // FEM_UTILS_FEM_UTILS_H