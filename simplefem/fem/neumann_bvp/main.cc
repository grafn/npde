/**
 * @file
 * @brief NPDE neumann bvp example
 * The problem solved in this code
 * \delta u + u = (1 + 4 \pi^2) cos(2 \pi x1) cos(2 \pi x2)
 * with boundary conditions: grad u = 0 on \partial \Omega
 *
 * which corresponds to the variational formulation
 * 
 * \int_{\Omega} grad u grad v + u v dx = \int (1 + 4 \pi^2) cos(2 \pi x1) cos(2 \pi x2) v dx & \forall v \in H_1
 * @author Nico Graf
 */

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/src/Core/util/Constants.h>
#include <iostream>
#include <lf/assemble/assemble.h>
#include <lf/assemble/fix_dof.h>
#include <lf/base/base.h>
#include <lf/geometry/geometry_interface.h>
#include <lf/mesh/mesh_factory.h>
#include <lf/mesh/utils/codim_mesh_data_set.h>
#include <memory>
#include <fem_utils.h>
#include <lf/uscalfe/uscalfe.h>
#include <Eigen/Sparse>
#include <lf/assemble/assemble.h>
#include <lf/assemble/coomatrix.h>
#include <lf/mesh/utils/utils.h>

int main() {

  /* 
    First we need some mesh which is assumend to be given for this course
    So there is not need to worry in this part of the code which might
    look strange
  */
  std::string meshfile = CMAKE_FEM_SOURCE_DIR "/meshes/Square1.txt";
  std::shared_ptr<lf::mesh::Mesh> mesh_p = FemUtils::read_mesh(meshfile);

  std::cout << "Loaded Mesh with " << mesh_p->NumEntities(0) << " Cells"
            << std::endl;
  
  /*
    Next we define the right space 
    This is we choose the space spanned by the piecewise linear basis functions
  */
  auto fe_space =
      std::make_shared<lf::uscalfe::FeSpaceLagrangeO1<double>>(mesh_p);
  
  /*
    Then we look for the right Bilinear Forms (using the element Matrix Providers) 
  */

  // Define lambda functions
  auto alpha_ = [](Eigen::Vector2d) -> double{
    return 1;
  };
  auto gamma_ = [](Eigen::Vector2d) -> double{
    return 1;
  };
  // make the right format of the lambda functions
  lf::mesh::utils::MeshFunctionGlobal<decltype(gamma_)> gamma(gamma_);
  lf::mesh::utils::MeshFunctionGlobal<decltype(alpha_)> alpha(alpha_);
  
  // Create the Matrix Provider
  lf::uscalfe::ReactionDiffusionElementMatrixProvider<double, decltype(alpha), decltype(gamma)> mat_prov(fe_space, alpha, gamma);
  

  /*
   * Now we define the source term (i.e. righhandside)
   * note that the neumann boundary conditions must also be included in this function!!
   */
  const double pi = 3.1415926535897;
  auto f_ = [pi](Eigen::Vector2d x) -> double{
    return (1. + 4. * pi * pi) * std::cos(2. * pi * x(0)) * cos(2. * pi * x(1));
  };
  lf::mesh::utils::MeshFunctionGlobal<decltype(f_)> f(f_);
  lf::uscalfe::ScalarLoadElementVectorProvider<double, decltype(f)> load_vector_prov(fe_space, f);
  
  /*
  * All that's left to do now is build the linear system 
  * We call this step the assemby and this is what you are sometimes asked to do by hand...
  */
  
  // Compute the Galerkin Matrix
  lf::assemble::COOMatrix<double> M_plus_A_COO = lf::assemble::AssembleMatrixLocally<lf::assemble::COOMatrix<double>>(0, fe_space->LocGlobMap(), fe_space->LocGlobMap(), mat_prov);
  
  // Compute the righthandside 
  Eigen::VectorXd Psi = lf::assemble::AssembleVectorLocally<Eigen::VectorXd>(0, fe_space->LocGlobMap(), load_vector_prov);
  
  // Solve Sparse Matrix System
  Eigen::SparseMatrix<double> M_plus_A = M_plus_A_COO.makeSparse();
  Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
  solver.compute(M_plus_A);
  if(solver.info() != Eigen::Success){
    std::cout << "Failed decomposing the Galerkin Matrix";
    return 1;
  }
  
  Eigen::VectorXd mu = solver.solve(Psi);
  std::cout << "Compute the solution with " << mu.size() << " basis coefficients \n";
  
  // Draw solution
  std::cout << "Created plot in file solution.vtk\nYou can find it in the build directory in the same folder as the executable! and plot it with paraview for example";
  
  return 0;
}
