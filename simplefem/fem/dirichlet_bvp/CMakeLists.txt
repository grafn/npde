project(dirichlet_bvp)

# define executable
add_executable(dirichlet_bvp.main main.cc)
 
# link executable with LehrFEM++ module
target_link_libraries(dirichlet_bvp.main Eigen3::Eigen LF::lf.base LF::lf.geometry 
    LF::lf.mesh.hybrid2d LF::lf.mesh LF::lf.mesh.utils LF::lf.uscalfe LF::lf.assemble LF::lf.io LF::lf.fe fem_utils.utils) 

