/**
 * @file
 * @brief NPDE dirichlet bvp example
 * The problem solved in this code
 * \delta u + u = (1 + 4 \pi^2) cos(2 \pi x1) cos(2 \pi x2)
 * with boundary conditions: u = sin(x1) on \partial \Omega
 *
 * which corresponds to the variational formulation
 * 
 * \int_{\Omega} grad u grad v + u v dx = \int (1 + 4 \pi^2) cos(2 \pi x1) cos(2 \pi x2) v dx & \forall v \in H_1
 * with boundary conditions: u = sin(x1) on \partial \Omega
 * @author Nico Graf
 */

#include <Eigen/Core>
#include <Eigen/Sparse>
#include <Eigen/src/Core/VectorBlock.h>
#include <Eigen/src/Core/util/Constants.h>
#include <iostream>
#include <lf/assemble/assemble.h>
#include <lf/assemble/fix_dof.h>
#include <lf/base/base.h>
#include <lf/geometry/geometry_interface.h>
#include <lf/mesh/entity.h>
#include <lf/mesh/mesh_factory.h>
#include <lf/mesh/utils/codim_mesh_data_set.h>
#include <memory>
#include <fem_utils.h>
#include <lf/uscalfe/uscalfe.h>
#include <Eigen/Sparse>
#include <lf/assemble/assemble.h>
#include <lf/assemble/coomatrix.h>
#include <lf/mesh/utils/utils.h>

int main() {

  /* 
    First we need some mesh which is assumend to be given for this course
    So there is not need to worry in this part of the code which might
    look strange
  */
  std::string meshfile = CMAKE_FEM_SOURCE_DIR "/meshes/Square1.txt";
  std::shared_ptr<lf::mesh::Mesh> mesh_p = FemUtils::read_mesh(meshfile);

  std::cout << "Loaded Mesh with " << mesh_p->NumEntities(0) << " Cells"
            << std::endl;
  
  /*
    Next we define the right space 
    This is we choose the space spanned by the piecewise linear basis functions
  */
  auto fe_space =
      std::make_shared<lf::uscalfe::FeSpaceLagrangeO1<double>>(mesh_p);
  
  /*
    Then we look for the right Bilinear Forms (using the element Matrix Providers) 
  */

  // Define lambda functions
  auto alpha_ = [](Eigen::Vector2d) -> double{
    return 1;
  };
  auto gamma_ = [](Eigen::Vector2d) -> double{
    return 1;
  };
  // make the right format of the lambda functions
  lf::mesh::utils::MeshFunctionGlobal<decltype(gamma_)> gamma(gamma_);
  lf::mesh::utils::MeshFunctionGlobal<decltype(alpha_)> alpha(alpha_);
  
  // Create the Matrix Provider
  lf::uscalfe::ReactionDiffusionElementMatrixProvider<double, decltype(alpha), decltype(gamma)> mat_prov(fe_space, alpha, gamma);
  

  /*
   * Now we define the source term
  */
  const double pi = 3.1415926535897;
  auto f_ = [pi](Eigen::Vector2d x) -> double{
    return (1. + 4. * pi * pi) * std::cos(2. * pi * x(0)) * cos(2. * pi * x(1));
  };
  lf::mesh::utils::MeshFunctionGlobal<decltype(f_)> f(f_);
  lf::uscalfe::ScalarLoadElementVectorProvider<double, decltype(f)> load_vector_prov(fe_space, f);
  
  /*
  * All that's left to do now is build the linear system 
  * We call this step the assemby and this is what you are sometimes asked to do by hand...
  */
  
  // Compute the Galerkin Matrix
  lf::assemble::COOMatrix<double> M_plus_A_COO = lf::assemble::AssembleMatrixLocally<lf::assemble::COOMatrix<double>>(0, fe_space->LocGlobMap(), fe_space->LocGlobMap(), mat_prov);
  
  // HERE THE CODE DIFFERS FORM THE SIMPLER NEUMANN CONDITION!
  // Create dirichlet boundary condition according to specification
  // We assume the boundary condition to be u(x) = sin(x_1) on \partial \Omega
  lf::mesh::utils::CodimMeshDataSet<bool> bd_flags{lf::mesh::utils::flagEntitiesOnBoundary(mesh_p, 2)};
  auto bd_cond = [&](unsigned int idx) -> std::pair<bool, double>{

    // First we get the point at which we want to enforce the value
    const lf::mesh::Entity *vertex = mesh_p->EntityByIndex(2,idx);
    
    // get the coordinates of the vertex
    const Eigen::Vector2d coords = lf::geometry::Corners(*(vertex->Geometry()));
    
    // get the value of sin(x1)
    double value = sin(coords(0));
    
    // check if the current vertex is on the boundary
    bool is_boundary = bd_flags((*vertex));

    return std::make_pair(is_boundary, value);
  };
  // FROM HERE WE AGAIN HAVE THE SAME CODE AS IN NEUMANN BOUNDARY CONDITIONS
  
  // Compute the righthandside 
  Eigen::VectorXd Psi = lf::assemble::AssembleVectorLocally<Eigen::VectorXd>(0, fe_space->LocGlobMap(), load_vector_prov);
  
  // HERE THE CODE DIFFERS FORM THE SIMPLER NEUMANN CONDITION!
  // Enforce Dirichlet Boundary Conditions in the matrix and the righthandside vector
  lf::assemble::FixFlaggedSolutionComponents(bd_cond, M_plus_A_COO, Psi);
  // FROM HERE WE AGAIN HAVE THE SAME CODE AS IN NEUMANN BOUNDARY CONDITIONS

  // Solve Sparse Matrix System
  Eigen::SparseMatrix<double> M_plus_A = M_plus_A_COO.makeSparse();
  Eigen::SparseLU<Eigen::SparseMatrix<double>> solver;
  solver.compute(M_plus_A);
  if(solver.info() != Eigen::Success){
    std::cout << "Failed decomposing the Galerkin Matrix";
    return 1;
  }
  
  Eigen::VectorXd mu = solver.solve(Psi);
  std::cout << "Compute the solution with " << mu.size() << " basis coefficients \n";
  
  // Draw solution
  std::cout << "Created plot in file solution.vtk\nYou can find it in the build directory in the same folder as the executable! and plot it with paraview for example";
  
  return 0;
}
