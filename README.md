# NPDE Tutorial Class

This Repository contains NONOFFICIAL additional material to the [Lecture Document](https://people.math.ethz.ch/~grsam/NUMPDEFL/NUMPDE.pdf).
Moreover there might be some basic examples and in class exercise codes (which I may add during the semster).
If you have any questions or concerns regarding anything in this repository or
the course in general, please send me an email <grafn@student.ethz.ch>.

Note though that this repository does not contain official course content and
hence is not relevant for the exam. But I will try to make it as closely related
as possible since this is the purpose of the tutorial classes I guess.

## Additional Summaries

There is a folder which contains the material I created for last years course. Note that I will not update this file. So it is up to you to decide whether the material in the document is covered in this years cours.

For this years version of the course we have a NONOFFICIAL short overview of the "most important" theory in the following polybox.
[Polybox Summary](https://polybox.ethz.ch/index.php/s/UFyt9grdn3j346H)

### Mid- / End-term

In the following link you find the Endterm exams of previous years.
[Exams NumPDE](https://people.math.ethz.ch/~grsam/NUMPDEFL/PastExams)


### Get up and running

Clone the git repository

```
git clone https://gitlab.ethz.ch/grafn/npde.git
```

Now you should have all data on your computer.


![Repository Website](qr.svg)
